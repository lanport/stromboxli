var Promise = require('bluebird');

exports.readVoltageL1 = function () {
	return new Promise(function (resolve) {
		resolve(230);
	});
}

exports.readVoltageL2 = function () {
	return new Promise(function (resolve) {
		resolve(231);
	});
}