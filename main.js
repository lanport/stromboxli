var Promise = require('bluebird');
var fs = require('fs');
var multimeter = require('./serial');

const PATH = 'data.csv';
const SEPARATOR = ';';

var promises = [];

(function loop() {
	var headers = [
		'Timestamp',
		'Datum',
		'Spannung L1',
		'Spannung L2',
		'Spannung L3',
		'Strom L1',
		'Strom L2',
		'Strom L3'
	];
	
	var timestamp = Math.floor(Date.now() / 1000);
	var date = new Date();
	date = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear() + ' - ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	var values = [timestamp, date];

	addData(multimeter.readVoltageL1, values);
	addData(multimeter.readVoltageL2, values);
	addData(multimeter.readVoltageL3, values);
	addData(multimeter.readCurrentL1, values);
	addData(multimeter.readCurrentL2, values);
	addData(multimeter.readCurrentL3, values);	

    Promise.all(promises).then(() => {
		var stats = fs.stat(PATH, err => {
			try {
				if(err) {
					writeToFile(createCsvLine(headers));
				}
				writeToFile(createCsvLine(values));
				
				logToConsole(headers, values)
			} catch(err) {
				logError(err);
			}
		});
	}).catch(err => {
		logError(err);
    }).finally(() => {
        setTimeout(loop);
    })
})()

function addData(func, values) {
	promises.push(func().then(data => {
		values.push(data);
    }));
}

function createCsvLine(array) {
	var str = '';
	array.forEach(row => {
		str += row + SEPARATOR;
	});
	str = str.slice(0, -1)
	return str + '\n';
}

function writeToFile(line) {
	fs.appendFileSync(PATH, line);
}

function logToConsole(headers, values) {
	for(var i = 0; i < headers.length; i++) {
		console.log(headers[i] + ': ' + values[i]);
	}
	console.log();
}

function logError(err) {
	console.log(err);
	console.log();
}