var SerialPort = require('serialport');
var modbus = require('modbus-rtu');
var Promise = require('bluebird');

const DEVICE_ADDRESS = 1;

var serialPort = new SerialPort("COM3", {
   baudRate: 115200,
   stopBits: 2
});

var master = new modbus.Master(serialPort, {
   endPacketTimeout: 400,
   queueTimeout: 400,
   responseTimeout: 500
});

exports.readVoltageL1 = function () {
	return read(19000)
}

exports.readVoltageL2 = function () {
	return read(19002)
}

exports.readVoltageL3 = function () {
	return read(19004)
}

exports.readCurrentL1 = function () {
	return read(19012)
}

exports.readCurrentL2 = function () {
	return read(19014)
}

exports.readCurrentL3 = function () {
	return read(19016)
}

function read(address) {
	return master.readHoldingRegisters(DEVICE_ADDRESS, address, 2, rawBuffer => {
		return Number((rawBuffer.readFloatBE(rawBuffer)).toFixed(2));
	});
}